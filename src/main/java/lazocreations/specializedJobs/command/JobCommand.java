package lazocreations.specializedJobs.command;

import lazocreations.specializedJobs.Main;
import lazocreations.specializedJobs.data.PlayerDB;
import lazocreations.specializedJobs.data.PlayerJob;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class JobCommand implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if(!(sender instanceof Player)) {
            sender.sendMessage("Console can't use this command!");
            return true;
        }
        
        Player player = (Player) sender;
        
        if(args.length < 1) {
            sender.sendMessage("/job help : Print SpecialJobs commands list.");
            return true;
        }
        
        switch(args[0].toLowerCase()) {
            
            case "help": {
                sender.sendMessage(new String[] {
                        "/job debug [on/off] : Toggle or show the state of your DebugMode.",
                        "/job repairCost : Print the repair cost of the item in your main-hand."
                });
                return true;
            }
            
            case "debug": {
                UUID id = player.getUniqueId();
                PlayerDB db = PlayerDB.getInstance();
                
                if(args.length < 2) {
                    boolean debug = db.getDebug(id);
                    sender.sendMessage("SpecialJobs debug-mode: " + debug);
                }
                else {
                    boolean debug = false;
                    
                    switch(args[1].toLowerCase()) {
                        case "on":
                        case "true":
                            debug = true;
                            break;
                        case "off":
                        case "false":
                            debug = false;
                            break;
                    }
                    
                    db.setDebug(id, debug);
                    sender.sendMessage("Debug mode has been set to " + args[1].toUpperCase());
                }
                return true;
            }
            
            case "repaircost": {
                ItemStack item = player.getInventory().getItemInMainHand();

                if (item == null || item.getType() == Material.AIR) {
                    sender.sendMessage("Above all, Hold the item you want!");
                    return true;
                }

                int cost = Main.anvilListener.getRepairCost(PlayerDB.getInstance().getAbilityGrade
                        (player.getUniqueId(), PlayerJob.Ability.ANVIL_REPAIR), item);

                sender.sendMessage("Your item's repair cost: " + cost);
                return true;
            }
            
            default:
                return false;
            
        }
        
    }
    
}