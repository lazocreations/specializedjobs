package lazocreations.specializedJobs.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import lazocreations.specializedJobs.Main;
import lazocreations.specializedJobs.data.PlayerDB;
import lazocreations.specializedJobs.data.PlayerJob;
import lazocreations.specializedJobs.exception.SkillDataCorruptedException;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

public class EnchantListener implements Listener {
    
    private HashMap<UUID, Integer> slot_tmp = new HashMap<>();
    private Main main = Main.getPlugin(Main.class);
    
    public EnchantListener() {
        
        PacketType packetType = PacketType.Play.Client.ENCHANT_ITEM;
        ProtocolManager protocol = ProtocolLibrary.getProtocolManager();
        protocol.addPacketListener(new PacketAdapter(Main.getPlugin(Main.class), packetType) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                
                if(event.getPacketType() != packetType) {
                    return;
                }
                
                PacketContainer packet = event.getPacket();
                slot_tmp.put(event.getPlayer().getUniqueId(), packet.getIntegers().read(1));
            }
        });
    
    }
    
    @EventHandler
    @SuppressWarnings("unchecked")
    public void onEnchant(EnchantItemEvent event) {
        
        Player player = (Player) event.getView().getPlayer();
        Integer slot = slot_tmp.get(player.getUniqueId());
        PlayerJob.Ability skill = PlayerJob.Ability.ENCHANTING;
        int grade = PlayerDB.getInstance().getAbilityGrade(player.getUniqueId(), skill);
        Double chance = getChance(grade, slot);
    
        if(chance == null) {
            try {
                throw new SkillDataCorruptedException(grade, skill.toString());
            } catch (SkillDataCorruptedException e) {
                e.printStackTrace();
            }
            event.setCancelled(true);
            return;
        }
        
        Main.debugPlayer(player, "Chances to succeed: " + chance);
        
        final boolean SUCCESS = Math.random() < chance;
        final Map<Enchantment, Integer> ENCHANTS = event.getEnchantsToAdd();
        final Block BLOCK = event.getEnchantBlock();
        final World WORLD = BLOCK.getWorld();
        final Item ITEM = WORLD.dropItem(BLOCK.getLocation().add(0.5, 0.5, 0.5), event.getItem());
        ITEM.setGravity(false);
        ITEM.setPickupDelay(150);
        
        Inventory inv = event.getView().getTopInventory();
        ItemStack lapis = inv.getItem(1);
        
        if(lapis != null && lapis.getType() != Material.AIR) {
            lapis.setAmount(lapis.getAmount() - slot - 1);
    
            if (lapis.getType() != Material.AIR) {
                for (ItemStack i : player.getInventory().addItem(lapis).values()) {
                    WORLD.dropItemNaturally(player.getLocation(), i);
                }
            }
        }
        
        inv.clear();
        player.closeInventory();
        
        new BukkitRunnable() {
            Vector vel = new Vector(0, 0.03, 0);
            Vector freeze = new Vector(0, 0, 0);
            boolean firstRun = false;
            int runTime = 0;
            int theta = 0;
            final float radius = 0.6F;
            final int thetaJump = 20;
            double dY = 0;
            Location eParLoc;
            boolean success = SUCCESS;
            
            @Override
            public void run() {
    
                Location loc = ITEM.getLocation();
                
                if(runTime++ < 60) {
                    double radian = Math.toRadians(theta);
                    double x = Math.cos(radian) * radius;
                    double y = Math.sin(radian) * radius;
                    
                    if((theta += thetaJump) > 360) {
                        theta = 0;
                    }
    
                    ITEM.setVelocity(vel);
                    WORLD.spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add(x, 0, y), 1);
                }
                
                else {
                    ITEM.setVelocity(freeze);
                    
                    if(runTime == 100) {
                        World world = ITEM.getWorld();
                        ItemStack is = ITEM.getItemStack();
                        ItemMeta meta = is.getItemMeta();
                        Particle particle;
                        
                        for(Map.Entry<Enchantment, Integer> ent : ENCHANTS.entrySet()) {
                            Enchantment ench = ent.getKey();
                            int level = ent.getValue();
                            int max = ench.getMaxLevel();
                            
                            if(max > 3) {
                                float n = level / max;
                                
                                if(n > 1/2 && Math.random() < (double) (2/3 * n)) {
                                    level--;
                                }
                            }
                            
                            else if(ench == Enchantment.SWEEPING_EDGE && Math.random() < level / max * 0.8) {
                                level--;
                            }
                            
                            if(level > 0) {
                                meta.addEnchant(ench, level, true);
                            }
                        }
                        
                        if(meta.getEnchants().size() < 1) {
                            success = false;
                        }
                        
                        if(is.getType() == Material.BOOK) {
                            is.setType(Material.ENCHANTED_BOOK);
                        }
                        
                        is.setItemMeta(meta);
                        ITEM.setItemStack(is);
    
                        if (success) {
                            particle = Particle.CRIT_MAGIC;
                        } else {
                            particle = Particle.CRIT;
                        }
                        
                        world.spawnParticle(particle, loc, 8, 0.5D, 0.5D, 0.5D);
                        world.spawnParticle(Particle.CRIT_MAGIC, loc, 3, 0.5D, 0.5D, 0.5D);
                    }
                    
                    else if(runTime > 140) {
                        
                        if(success) {
                            WORLD.playSound(loc, Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
                            player.sendMessage("Succeed to enchant the item!");
                        }
                        
                        else {
                            ItemStack is = ITEM.getItemStack();
                            Material type = is.getType();
                            boolean isBook = type == Material.ENCHANTED_BOOK;
                            short addDur = (short) (type.getMaxDurability() * Math.random());
                            is.setItemMeta(null);
                            
                            if(!isBook && addDur + is.getDurability() < (short) (type.getMaxDurability() * 0.8)) {
                                is.setDurability((short) (is.getDurability() + addDur));
                            }
                            else if(!isBook || new Random().nextBoolean()) {
                                ITEM.remove();
                            }
                            else {
                                is.setType(Material.BOOK);
                            }
    
                            ITEM.setItemStack(is);
                            WORLD.playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 0.6F, 1.2F);
                            WORLD.spawnParticle(Particle.EXPLOSION_NORMAL, loc, 2, 0.2D, 0.2D, 0.2D);
                            WORLD.spawnParticle(Particle.EXPLOSION_LARGE, loc, 1);
                            player.sendMessage("You failed the enchantment.");
                        }
    
                        ITEM.setGravity(true);
                        this.cancel();
                    }
                }
    
                if(runTime > 30) {
                    if(dY == 0 && eParLoc == null) {
                        dY = loc.getY() - BLOCK.getY();
                        eParLoc = BLOCK.getLocation().clone().add(0.5, 0, 0.5);
                        WORLD.playSound(loc, Sound.BLOCK_PORTAL_TRIGGER, 1.0F, 0.9F);
                    }
        
                    else if(runTime < 100) {
                        eParLoc = eParLoc.add(0, (double) 1/60 * dY, 0);
                        WORLD.spawnParticle(Particle.ENCHANTMENT_TABLE, eParLoc, 2);
                    }
                }
                
            }
        }.runTaskTimer(main, 1L, 1L);
        
    }
    
    @SuppressWarnings("unchecked")
    private Double getChance(int grade, int slot) {
        Double chance = null;
        List<Map<?, ?>> mapList = Main.config.getMapList("skills.ENCHANTING");
    
        for(Map<?, ?> map : mapList) {
            int grade1 = (Integer) map.get("grade");
        
            if(grade == grade1) {
                chance = ((List<Double>) map.get("success")).get(slot);
            }
        }
        
        return chance;
    }
    
}