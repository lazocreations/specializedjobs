package lazocreations.specializedJobs.data;

import lazocreations.specializedJobs.Main;
import lazocreations.specializedJobs.exception.SkillDataCorruptedException;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SkillDB {
    
    private static final SkillDB INSTANCE;
    public double[][] a; // weapon durability
    public double[][] b; // tool durability
    
    static {
        INSTANCE = new SkillDB();
    }
    
    public static SkillDB getInstance() {
        return INSTANCE;
    }
    
    private SkillDB() {
        readDurability(PlayerJob.Ability.WEAPON_DURABILITY);
        readDurability(PlayerJob.Ability.TOOL_DURABILITY);
    }
    
    @SuppressWarnings("unchecked")
    private void readDurability(PlayerJob.Ability skillType) {
        List<Map<? ,?>> mapList = Main.config.getMapList("skills." + skillType.toString());
        Iterator<Map<?, ?>> iter = mapList.iterator();
        
        double[][] discount = new double[mapList.size()][6];
        
        while(iter.hasNext()) {
            Map<String, Object> map = (Map<String, Object>) iter.next();
            List<Object> arr = (List<Object>) map.get("discount");
            int grade = (int) map.get("grade");
            int count = 0;
            
            for(Object val : arr) {
                if(val instanceof String) {
                    val = Double.parseDouble((String) val);
                }
                
                try {
                    discount[5 - grade][count++] = (Double) val;
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    Main.logger.severe("Some rows in \'discount\' have more than 6 columns.");
                } catch (ClassCastException e1) {
                    e1.printStackTrace();
                    Main.logger.severe("Some columns in \'discount\' don't have numeric values.");
                }
            }
            
        }
        
        if(skillType == PlayerJob.Ability.WEAPON_DURABILITY) {
            a = discount;
        } else {
            b = discount;
        }
    }
    
}