package lazocreations.specializedJobs.listeners;

import com.gmail.nossr50.events.skills.alchemy.McMMOPlayerBrewEvent;
import com.gmail.nossr50.events.skills.alchemy.McMMOPlayerCatalysisEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class McMMOListener implements Listener {
    
    @EventHandler
    public void onPlayerBrew(McMMOPlayerBrewEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler
    public void onCatalysis(McMMOPlayerCatalysisEvent event) {
        event.setCancelled(true);
    }
    
}