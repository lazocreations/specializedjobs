package lazocreations.specializedJobs.data;

public enum PlayerJob {
	NONE,
	SOLDIER,
	ENGINEER,
	COLLECTOR;
	
	public enum Ability {
		ANVIL_REPAIR,
		ANVIL_BOOK,
		WEAPON_DURABILITY,
		TOOL_DURABILITY,
		ENCHANTING,
		BREWING,
		CRAFTING,
		LOOT,
		HUNGER,
		FARMING;
		
		public static PlayerJob getMasterJob(Ability ability) {
			switch(ability) {
				case WEAPON_DURABILITY:
				case LOOT:
				case HUNGER:
					return SOLDIER;
						
				case ANVIL_REPAIR:
				case ANVIL_BOOK:
				case ENCHANTING:
				case BREWING:
				case CRAFTING:
					return ENGINEER;
						
				case TOOL_DURABILITY:
				case FARMING:
					return COLLECTOR;
						
				default:
					return NONE;
			}
		}
		
		public PlayerJob getMasterJob() {
			return Ability.getMasterJob(this);
		}
		
	}
	
}
