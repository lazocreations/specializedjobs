package lazocreations.specializedJobs.listeners;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import lazocreations.specializedJobs.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.UUID;

public class InventoryListener implements Listener {
    
    private HashMap<UUID, BukkitRunnable> craftPlayers = new HashMap<>();
    
    @EventHandler
    public void onClick(InventoryClickEvent event) {
    	InventoryType type = event.getView().getType();
        
    	// TODO simplify the call to every single inventory-related listener
        switch(type) {
            case ANVIL:
                Main.anvilListener.onClick(event);
                break;
            case BREWING:
                Main.brewListener.onClick(event);
                break;
            case WORKBENCH: {
                InventoryView view = event.getView();
                Player player = (Player) view.getPlayer();
    
                if(view.convertSlot(event.getRawSlot()) == event.getRawSlot()
                        && craftPlayers.containsKey(player.getUniqueId())) {
                    BountifulAPI.sendActionBar(player, "Crafting cooldown is still remaining.", 30);
                }
                break;
            }
            default:
                break;
        }
    }
    
    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        
        InventoryType type = event.getView().getType();
        
        switch(type) {
            case ANVIL:
                Main.anvilListener.onDrag(event);
                break;
            case BREWING:
                Main.brewListener.onDrag(event);
                break;
            default:
            	break;
        }
        
    }
    
    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        
        Inventory inv = event.getInventory();
        
        if(inv.getType() == InventoryType.WORKBENCH) {
            Player player = (Player) event.getPlayer();
            
            if(craftPlayers.get(player.getUniqueId()) != null) {
                BountifulAPI.sendActionBar(player, "Crafting cooldown is still remaining.", 60);
            }
        }
        
    }
    
    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        
        InventoryType type = event.getInventory().getType();
        
        switch (type) {
            case ANVIL:
                Main.anvilListener.onClose(event);
                break;
        }
        
    }
    
    @EventHandler
    public void onItemCraft(CraftItemEvent event) {
        
        CraftingInventory inv = event.getInventory();
        Player player = (Player) event.getWhoClicked();
        final UUID playerId = player.getUniqueId();
        final short coolTime = 1200;
        double penalty;
        BukkitRunnable task = craftPlayers.get(playerId);
        
        if(inv.getType() != InventoryType.WORKBENCH) {
            return;
        }
        
        if(task != null) {
            task.cancel();
            penalty = 0.3D;
        }
        else {
            penalty = 0.08D;
        }
        
        BukkitRunnable run = new BukkitRunnable() {
            @Override
            public void run() {
                craftPlayers.remove(playerId);
            }
        };
        run.runTaskLater(Main.getPlugin(Main.class), coolTime);
        craftPlayers.put(playerId, run);
        
        if(Math.random() < penalty) {
            
            ItemStack[] newMatrix = new ItemStack[9];
            ItemStack item = event.getRecipe().getResult();
            String msg;
            int multiply = 1;
            int matrixNum = 0;
            
            for(ItemStack m : inv.getMatrix()) {
                if(m.getType() == Material.AIR) continue;
                
                int amount = m.getAmount();
                
                if(player.isSneaking() && multiply > amount) {
                    multiply = amount;
                }
            }
            
            for(ItemStack m : inv.getMatrix()) {
                if(m.getType() == Material.AIR) continue;
                
                m.setAmount(m.getAmount() - multiply);
                newMatrix[matrixNum++] = m;
            }
            
            item.setAmount(item.getAmount() * multiply);
            
            if(item.getType().getMaxDurability() > 100) {
                item.setDurability((short) (item.getType().getMaxDurability() * Math.random()));
            }
            else {
                item.setAmount(item.getAmount() - multiply);
            }
            
            World world = player.getWorld();
            Location loc = inv.getLocation();
            ItemStack piece = item.clone();
            ItemMeta pieceMeta = piece.getItemMeta();
            String display = "\u00A7d\u00A7r\u00A7o";
            piece.setAmount(1);
            
            for(int i=0; i<item.getAmount(); i++) {
                pieceMeta.setDisplayName(display + i);
                piece.setItemMeta(pieceMeta);
                
                Item item1 = world.dropItemNaturally(loc, piece);
                item1.setVelocity(item1.getVelocity().multiply(1).setY(0.5));
                item1.setPickupDelay(50);
            }
            
            inv.setMatrix(newMatrix);
            inv.setResult(null);
            player.updateInventory();
            player.closeInventory();
            
            if(item.getAmount() > 0) {
                BountifulAPI.sendActionBar(player, "You made a mistake while crafting.", 300);
            }
            else {
                player.sendMessage("You failed to craft the item.");
            }
            
        }
        
    }
    
    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent event) {
        Item item = event.getItem();
        ItemMeta meta = item.getItemStack().getItemMeta();
        String display = null;
        
        if(meta != null) {
            display = meta.getDisplayName();
        }
        
        if (display != null && display.startsWith("\u00A7d\u00A7r\u00A7o")) {
            ItemStack stack = item.getItemStack();
            stack.setItemMeta(null);
            item.setItemStack(stack);
        }
        
    }
    
}