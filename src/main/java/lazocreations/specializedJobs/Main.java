package lazocreations.specializedJobs;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import lazocreations.specializedJobs.command.JobCommand;
import lazocreations.specializedJobs.data.PlayerDB;
import lazocreations.specializedJobs.listeners.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public final class Main extends JavaPlugin {
    
    public static Logger logger;
    public static PlayerListener playerListener;
    public static AnvilListener anvilListener;
    public static EnchantListener enchantListener;
    public static BrewListener brewListener;
    public static InventoryListener inventoryListener;
    public static FileConfiguration config;
    private static Main instance;
    
    @Override
    public void onEnable() {
    	
    	logger = getLogger();
    	instance = this;
    	loadConfig();
        
        PluginManager plugin = getServer().getPluginManager();
        Plugin mcMMO = plugin.getPlugin("mcMMO");
        
        playerListener = new PlayerListener();
        anvilListener = new AnvilListener();
        enchantListener = new EnchantListener();
        brewListener = new BrewListener(mcMMO != null);
        inventoryListener = new InventoryListener();
        
        plugin.registerEvents(playerListener, this);
        plugin.registerEvents(anvilListener, this);
        plugin.registerEvents(enchantListener, this);
        plugin.registerEvents(brewListener, this);
        plugin.registerEvents(inventoryListener, this);
        
        getCommand("specialjob").setExecutor(new JobCommand());
        
        if(mcMMO != null) {
            plugin.registerEvents(new McMMOListener(), this);
        }
        
    }
    
    @Override
    public void onDisable() {
        
    }
    
    public static void debugPlayer(Player player, String... message) {
        
        if(PlayerDB.getInstance().getDebug(player.getUniqueId())) {
            player.sendMessage(message);
        }
        
    }
    
    public static void debugPlayer(Player player, String actionMsg) {
        
        if(PlayerDB.getInstance().getDebug(player.getUniqueId())) {
            BountifulAPI.sendActionBar(player, actionMsg, 20);
        }
        
    }
    
    public static Main getInstance() {
        return instance;
    }
    
    public void loadConfig() {
        saveDefaultConfig();
        Main.config = getConfig();
    }
    
}