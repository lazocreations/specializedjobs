package lazocreations.specializedJobs.exception;

import lazocreations.specializedJobs.Main;

public class SkillDataCorruptedException extends Exception {
    
    private int grade;
    private String skillName;
    
    public SkillDataCorruptedException(int grade, String skillName) {
        this.grade = grade;
        this.skillName = skillName;
    }
    
    @Override
    public void printStackTrace() {
        super.printStackTrace();
        Main.logger.severe("Failed to retrieve data with the following JobSkill - type: " + skillName + ", grade: " + grade);
    }
    
}