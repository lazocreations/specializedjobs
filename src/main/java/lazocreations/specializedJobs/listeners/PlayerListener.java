package lazocreations.specializedJobs.listeners;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import lazocreations.specializedJobs.Main;
import lazocreations.specializedJobs.data.PlayerDB;
import lazocreations.specializedJobs.data.PlayerJob;
import lazocreations.specializedJobs.data.SkillDB;
import lazocreations.specializedJobs.exception.SkillDataCorruptedException;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Random;
import java.util.logging.Level;

public class PlayerListener implements Listener {
    
	@EventHandler
	public synchronized void playerJoin(PlayerJoinEvent event) {
		PlayerDB db = PlayerDB.getInstance();
		Player player = event.getPlayer();
		
		if (!db.hasPlayerJob(player.getUniqueId())) {
			db.addPlayer(player.getUniqueId());
			Main.logger.log(Level.INFO, "Player " + player.getDisplayName() + " has been added to db.");
		}
        
        if(db.getDebug(player.getUniqueId())) {
            BountifulAPI.sendActionBar(player, "SpecialJobs DebugMode is ON.", 60);
        }
        
	}
	
    /*
        Modify the mob loot items.
     */
    @EventHandler
    public void playerKilledMob(EntityDeathEvent event) {
        
        LivingEntity victim = event.getEntity();
        Player player = victim.getKiller();
        
        if(player != null && victim.getType() != EntityType.PLAYER) {
            int count = 0;
            
            for(ItemStack item : event.getDrops()) {
                
                for(int i = 0; i < item.getAmount(); i++) {
                    
                    if (Math.random() < 0.7D) {
                        item.setAmount(item.getAmount() - 1);
                        count++;
                    }
                    
                }
                
            }
            
            Main.debugPlayer(player, "You just lost " + count + " items for loot.");
        }
    }
    
    /*
        Take more damage than usual to the player's tool.
     */
    @EventHandler
    public void onPlayerDamageTool(PlayerItemDamageEvent event) {
        
        if(event.isCancelled())
            return;
        
        ItemStack item = event.getItem();
        String type = item.getType().toString().toLowerCase();
        boolean isTool = type.contains("pickaxe") || type.contains("shovel");
        boolean isWeapon = type.contains("sword") || type.contains("axe");
        
        if(!(isTool || isWeapon))
            return;
        
        Integer unbreaking = 0;
        ItemMeta meta = item.getItemMeta();
        Player player = event.getPlayer();
        
        if(meta != null) {
            unbreaking = meta.getEnchants().get(Enchantment.DURABILITY);
            
            if(unbreaking == null) unbreaking = 0;
        }
        
        short maxDur = item.getType().getMaxDurability();
        PlayerJob.Ability skill = isTool ? PlayerJob.Ability.TOOL_DURABILITY : PlayerJob.Ability.WEAPON_DURABILITY;
        int grade = PlayerDB.getInstance().getAbilityGrade(player.getUniqueId(), skill);
        Short damage = getItemDamage(grade, isTool, unbreaking);
        
        if(damage == null) {
            try {
                throw new SkillDataCorruptedException(grade, skill.toString());
            } catch (SkillDataCorruptedException e) {
                e.printStackTrace();
            }
            return;
        }
        
        if(damage < 0)
            return;
        
        if(maxDur <= item.getDurability() + damage) {
            player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, (float) Math.random() + 0.5F);
            player.spawnParticle(Particle.ITEM_CRACK, player.getLocation(), 3);
        }
        
        event.setDamage(damage);
        
        Main.debugPlayer(player, "Damage amount: " + damage);
        
    }
    
    /*
        Appease the player's hunger.
     */
    @EventHandler
    public void hungerChange(FoodLevelChangeEvent event) {
        
        int food = event.getFoodLevel();
        
        if(!event.isCancelled() && food < 20 && food > 0) {
            
            Player player = (Player) event.getEntity();
            
            if(player.isSprinting() || !player.getInventory().getItemInMainHand().getType().isEdible()) {
                
                // The maximum chance to cancel hunger when hunger guage hits floor.
                double maxChance = 0.3D;
                double chance = (-maxChance / 20) * food + maxChance;
                
                if (Math.random() < chance) {
                    event.setCancelled(true);
                    Main.debugPlayer(player, "Hunger event cancelled.");
                }
                
            }
            
        }
        
    }
    
    private Short getItemDamage(int grade, boolean isTool, int unbreakingLevel) {
        
        Random rand = new Random();
        Double damage;
        double[][] array = isTool ? SkillDB.getInstance().b : SkillDB.getInstance().a;
        
        try {
            damage = array[5 - grade][unbreakingLevel];
        } catch(ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return null;
        }
        
        double floor = Math.floor(damage);
        
        if(Math.random() < damage - floor) {
            return (short) (floor + 1);
        }
        
        return (short) floor;
        
    }
    
}