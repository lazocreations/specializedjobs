package lazocreations.specializedJobs.listeners;

import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;
import com.connorlinfoot.bountifulapi.BountifulAPI;
import lazocreations.specializedJobs.Main;
import lazocreations.specializedJobs.data.PlayerDB;
import lazocreations.specializedJobs.data.PlayerJob;
import lazocreations.specializedJobs.exception.SkillDataCorruptedException;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitScheduler;

import java.lang.reflect.Method;
import java.util.*;

public class AnvilListener implements Listener {
    
    private static BukkitScheduler scheduler = Bukkit.getScheduler();
    private static HashMap<UUID, Double> playerChance = new HashMap<>();
    private static HashMap<UUID, Double> itemDurLoss = new HashMap<>();
    private static final int maxIngred = 5;
    
    @EventHandler
    @SuppressWarnings("unchecked")
    public void modifyRepair(PrepareAnvilEvent event) {
        
        Player player = (Player) event.getView().getPlayer();
        AnvilInventory inv = event.getInventory();
        ItemStack result = event.getResult();
        
        if (result == null || result.getType() == Material.AIR) {
            return;
        }
        
        // cont array - 0: tool slot, 1: ingredient slot, 2: result slot
        ItemStack[] cont = inv.getContents();
        ItemStack tool = cont[0];
        ItemStack ingredient = cont[1];
        Double extraDura = null, chance = null;
        PlayerJob.Ability skill;
        int grade = PlayerDB.getInstance().getAbilityGrade(player.getUniqueId(), PlayerJob.Ability.ANVIL_REPAIR);
        int costSum = getRawRepairCost(tool) + getRawRepairCost(ingredient);
        
        event.getInventory().setRepairCost(getRepairCost(grade, costSum));
        
        if (ingredient.getType() == tool.getType()) {
            return;
        }
        
        skill = ingredient.getType() == Material.ENCHANTED_BOOK ? PlayerJob.Ability.ANVIL_BOOK : PlayerJob.Ability.ANVIL_REPAIR;
        int ingredSize = ingredient.getAmount();
        int flow = ingredSize - maxIngred;
        
        if (flow > 0) {
            
            ingredient.setAmount(maxIngred);
            
            ItemStack retrieve = ingredient.clone();
            ItemStack cursor = player.getItemOnCursor();
            
            if(cursor.isSimilar(ingredient)) {
                flow += cursor.getAmount();
            }
            else if(cursor.getType() != Material.AIR) {
                player.getWorld().dropItemNaturally(player.getLocation(), cursor);
            }
            
            if(flow > 64) {
                flow -= 64;
                retrieve.setAmount(64);
                player.getWorld().dropItemNaturally(player.getLocation(), retrieve);
            }
    
            retrieve.setAmount(flow);
            player.setItemOnCursor(retrieve);
            inv.setContents(cont);
            return;
            
        }
        
        grade = PlayerDB.getInstance().getAbilityGrade(player.getUniqueId(), skill);
        List<Map<?, ?>> maps = Main.config.getMapList("skills." + skill.toString());
        
        for (Map<?, ?> map1 : maps) {
            Map<String, Object> map = (Map<String, Object>) map1;
        
            if (map.get("grade") == (Integer) grade) {
                
                List<Double> successes = (List<Double>) map.get("success");
                
                if(skill == PlayerJob.Ability.ANVIL_REPAIR) {
                    List<Double> extraDuras = (List<Double>) map.get("extra-durability");
                    
                    try {
                        chance = successes.get(ingredSize - 1);
                    } catch (IndexOutOfBoundsException e) {
                        chance = successes.get(successes.size() - 1);
                    }
                    
                    try {
                        extraDura = extraDuras.get(ingredSize - 1);
                    } catch (IndexOutOfBoundsException e) {
                        extraDura = extraDuras.get(extraDuras.size() - 1);
                    }
                }
                else {
                    chance = 1.0;
                }
            }
        }
        
        if (chance == null) {
            try {
                throw new SkillDataCorruptedException(grade, skill.toString());
            } catch (SkillDataCorruptedException e) {
                e.printStackTrace();
            }
            return;
        }
        
        if (skill == PlayerJob.Ability.ANVIL_REPAIR) {
            double dur = (tool.getDurability() - result.getDurability()) * (1D + extraDura);
            short prevDur = result.getDurability();
            short newDur = (short) (tool.getDurability() - dur);
            ItemMeta meta = result.getItemMeta();
            List<String> lore = new ArrayList<>();
            lore.add(0, "\u00A76\ub0b4\uad6c\ub3c4 \uc218\ub9ac: " + (int) (extraDura * 100D) + "%");
            lore.add(1, "\u00A7e\uc131\uacf5 \ud655\ub960: " + (int) (chance * 100D) + "%");
            meta.setLore(lore);
            
            if (prevDur < 1) {
                newDur = prevDur;
            }
            
            result.setItemMeta(meta);
            result.setDurability(newDur);
        }
        else {
            List<Map<?, ?>> mapList = Main.config.getMapList("skills.ANVIL_BOOK");
            
            for (Map<?, ?> map : mapList) {
                if ((Integer) map.get("grade") == grade) {
                    Object d = map.get("durability-loss");
                    double loss;
                    
                    if (d instanceof Double) {
                        loss = (Double) d;
                    } else if (d instanceof String) {
                        loss = Double.valueOf((String) d);
                    } else {
                        try {
                            throw new SkillDataCorruptedException(grade, skill.toString());
                        } catch (SkillDataCorruptedException e) {
                            e.printStackTrace();
                        }
                        event.setResult(null);
                        player.updateInventory();
                        return;
                    }
                    
                    short maxDur = result.getType().getMaxDurability();
                    short newDur = (short) (maxDur * loss + result.getDurability());
                    
                    if(newDur >= maxDur) {
                        event.setResult(null);
                        player.updateInventory();
                        BountifulAPI.sendActionBar(player, "\u00A7cThis item is damaged too much.", 40);
                        player.sendMessage("\u00A7cThis item is damaged too much.");
                        return;
                    }
                    
                    result.setDurability(newDur);
                    event.setResult(result);
                    player.updateInventory();
                    itemDurLoss.put(player.getUniqueId(), loss);
                }
            }
        }
        
        setRawRepairCost(result, getRawRepairCost(tool));
        event.setResult(result);
        playerChance.put(player.getUniqueId(), chance);
    }
    
    public void onClick(InventoryClickEvent event) {
        
        final Player player = (Player) event.getWhoClicked();
        int rawSlot = event.getRawSlot();
        
        if (rawSlot != event.getView().convertSlot(rawSlot)) {
            return;
        }
    
        AnvilInventory anvil = (AnvilInventory) event.getClickedInventory();
        ItemStack slotItem = event.getCurrentItem();
        ItemStack[] cont = anvil.getContents();
        UUID playerId = player.getUniqueId();
        
        if (rawSlot == 1) {
            if (cont[0] == null) { return; }
            ItemStack cursor = event.getCursor();
            
            if (cursor != null && cursor.isSimilar(slotItem)) {
                int slotCount = slotItem.getAmount();
                
                if (slotCount >= maxIngred) {
                    cursor.setAmount(cursor.getAmount() + slotCount);
                    player.setItemOnCursor(cursor);
                    event.setCurrentItem(null);
                    event.setCancelled(true);
                    scheduler.runTaskLater(Main.getPlugin(Main.class), player::updateInventory, 1L);
                }
            }
        }
        
        else if (rawSlot == 2 && slotItem != null && slotItem.getType() != Material.AIR) {
            // Player is retrieving result item (slotItem == cont[2])
    
            int rawCost = getRawRepairCost(slotItem);
            ItemStack ingred = cont[1];
            Material ingredType = ingred.getType();
            
            if (ingredType.equals(cont[0].getType())) {
                return;
            }
            
            if (!playerChance.containsKey(playerId)) {
                return;
            }
            
            Main main = Main.getPlugin(Main.class);
            PlayerJob.Ability skill = ingredType == Material.ENCHANTED_BOOK ?
                    PlayerJob.Ability.ANVIL_BOOK : PlayerJob.Ability.ANVIL_REPAIR;
            boolean damaged = false;
            String msg = null;
            Sound sound;
            Random rand = new Random();
            
            if(skill == PlayerJob.Ability.ANVIL_REPAIR) {
                slotItem = slotItem.clone();
                List<String> origin = anvil.getContents()[0].getItemMeta().getLore();
                ItemMeta meta = slotItem.getItemMeta();
                meta.setLore(origin);
                slotItem.setItemMeta(meta);
            }
            
            if (rand.nextFloat() < playerChance.get(playerId)) {
                /* Succeed to use anvil */
                
                setRawRepairCost(slotItem, ++rawCost);
                
                switch(skill) {
                    case ANVIL_REPAIR:
                        msg = "\u00A7aRepaired your tool!";
                        break;
                    case ANVIL_BOOK:
                        msg = "\u00A7aApplied enchantment to your tool! \u00A7c-" + itemDurLoss.get(playerId) * 100D + "% durability";
                        break;
                }
                sound = Sound.BLOCK_ANVIL_USE;
    
            } else { /* failed */
                
                if (skill == PlayerJob.Ability.ANVIL_REPAIR) {
                    short dura = slotItem.getDurability();
                    slotItem.setDurability((short) (dura + new Random().nextInt(slotItem.getType().getMaxDurability() - dura)));

                    if (slotItem.getDurability() > slotItem.getType().getMaxDurability()) {
                        slotItem = null;
                    }

                    msg = "\u00A7cFailed to repair your tool...";
                }
                else {
                    ItemMeta meta = ingred.getItemMeta();
                    meta.getEnchants().forEach((ench, level) -> {
                        meta.removeEnchant(ench);
                        
                        if(--level > 0) {
                            meta.addEnchant(ench, level, false);
                        }
                    });
                    
                    for(ItemStack is : player.getInventory().addItem(ingred).values()) {
                        player.getWorld().dropItemNaturally(player.getLocation(), is);
                    }
                    
                    ingred.setItemMeta(meta);
                    msg = "\u00A7cFailed to apply enchantment...";
                }
                
                sound = Sound.ENTITY_ITEM_BREAK;
            }
            
            if (slotItem != null && slotItem.getType() != Material.AIR) {
                for (ItemStack is : player.getInventory().addItem(slotItem).values()) {
                    player.getWorld().dropItemNaturally(player.getLocation(), is);
                }
            }
            
            BountifulAPI.sendActionBar(player, msg, 50);
            player.sendMessage(msg);
            player.playSound(player.getLocation(), sound, 1F, rand.nextFloat());
            
            try {
                damaged = damageAnvil(player, anvil.getLocation());
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            if (damaged) {
                if (anvil.getLocation().getBlock().getType() == Material.ANVIL) {
                    scheduler.runTaskLater(main, () -> {
                        BountifulAPI.sendActionBar(player, "\u00A7eYour anvil was damaged.", 50);
                        player.sendMessage("\u00A7eYour anvil was damaged.");
                    }, 50L);
                } else {
                    scheduler.runTaskLater(main, () -> {
                        BountifulAPI.sendActionBar(player, "\u00A7cYour anvil was broken..", 50);
                        player.sendMessage("\u00A7eYour anvil was broken..");
                    }, 50L);
                }
            }
            
            Main.debugPlayer(player, "New repair cost for the tool: " +
                    getRepairCost(PlayerDB.getInstance().getAbilityGrade(playerId, PlayerJob.Ability.ANVIL_REPAIR), rawCost));
            
            anvil.clear();
            event.setCancelled(true);
            event.getView().close();
        }
    }
    
    public void onDrag(InventoryDragEvent event) {
        InventoryView view = event.getView();
        InventoryType.SlotType slotType = InventoryType.SlotType.CONTAINER;
        ClickType clickType = ClickType.LEFT;
        InventoryAction action = InventoryAction.PLACE_ALL;
        Set<Integer> slots = event.getRawSlots();
        int initSlot = slots.iterator().next();
        
        for (int i : slots) {
            if (initSlot != i) {
                event.setCancelled(true);
                return;
            }
        }
        
        onClick(new InventoryClickEvent(view, slotType, initSlot, clickType, action));
    }
    
    public void onClose(InventoryCloseEvent event) {
        UUID playerId = event.getPlayer().getUniqueId();
        playerChance.remove(playerId);
        itemDurLoss.remove(playerId);
    }
    
    /**
     * Add damage to the specified anvil block.
     * @param player who are viewing anvil inventory.
     * @param loc anvil block's location
     * @return Whether the anvil was damaged by this.
     * @throws Exception Any exception may arise due to reflections with NMS classes.
     */
    @SuppressWarnings("unchecked")
    public static boolean damageAnvil(Player player, Location loc) throws Exception {
        
        Random rand = new Random();
        
        if (rand.nextFloat() > 0.12F) {
            return false;
        }
        
        //BlockPosition
        Object blockPos = MinecraftReflection.getBlockPositionClass()
                .getConstructor(Integer.TYPE, Integer.TYPE, Integer.TYPE)
                .newInstance(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        
        Object craftWorld = MinecraftReflection.getCraftWorldClass().cast(player.getWorld());
        
        Object worldServer = craftWorld.getClass().getDeclaredMethod("getHandle").invoke(craftWorld);
        
        // Actual object: net.minecraft.server.BlockStateList$BlockData
        Object blockData = worldServer.getClass().getMethod("getType", blockPos.getClass()).invoke(worldServer, blockPos);
        
        // if(world.isClientSide == false)
        if (!worldServer.getClass().getField("isClientSide").getBoolean(worldServer)
                && player.getGameMode() != GameMode.CREATIVE
                && loc.getBlock().getType() == Material.ANVIL) {
            
            Class blockAnvil = MinecraftReflection.getMinecraftClass("BlockAnvil");
            Object DAMAGE = blockAnvil.getField("DAMAGE").get(null);
            Method get = blockData.getClass().getDeclaredMethod("get",
                    MinecraftReflection.getMinecraftClass("IBlockState"));
            
            get.setAccessible(true);
            
            int dmg = (int) get.invoke(blockData, DAMAGE);
            ++dmg;
            
            if (dmg > 2) {
                loc.getBlock().setType(Material.AIR);
                loc.getWorld().playSound(loc, Sound.BLOCK_ANVIL_DESTROY, 1.0F, 1.0F);
                return true;
            }
            
            // boolean setTypeAndData(BlockPosition, IBlockData, int)
            Method setTypeAndData = worldServer.getClass().getMethod("setTypeAndData",
                    blockPos.getClass(), MinecraftReflection.getIBlockDataClass(), Integer.TYPE);
            
            // IBlockData
            Object newBlockData = null;
            for (Method m : blockData.getClass().getMethods()) {
                m.setAccessible(true);
                if (m.getName().equals("set")) {
                    newBlockData = m.invoke(blockData, DAMAGE, dmg);
                }
            }
            
            setTypeAndData.invoke(worldServer, blockPos, newBlockData, 2);
            
            return true;
            
        }
        
        return false;
        
    }
    
    public int getRepairCost(int grade, ItemStack item) {
        return getRepairCost(grade, getRawRepairCost(item));
    }
    
    public int getRepairCost(int grade, int rawCost) {
        int result = 0;
        
        if(rawCost < 1) {
            return 0;
        }
        
        switch(grade) {
            case 5: {
                for (int i = 0; i < rawCost; i++) {
                    result += Math.pow(2, i);
                }
                break;
            }
            
            case 4:
                result += Math.pow(2, rawCost - 1);
                break;
                
            case 3: {
                result += 1;
    
                for (int i = 0; i < rawCost; i++) {
                    result += i;
                }
                break;
            }
            
            case 2:
                result = 2 * (rawCost-1);
                break;
                
            case 1:
                result = rawCost;
                break;
                
            default:
                try {
                    throw new SkillDataCorruptedException(grade, PlayerJob.Ability.ANVIL_REPAIR.toString());
                } catch (SkillDataCorruptedException e) {
                    e.printStackTrace();
                }
                break;
        }
        
        return result;
    }
    
    private void setRawRepairCost(ItemStack item, int cost) {
        NbtCompound comp = (NbtCompound) NbtFactory.fromItemTag(item);
        comp.put("RepairCost", cost);
        NbtFactory.setItemTag(item, comp);
    }
    
    private int getRawRepairCost(ItemStack item) {
        NbtCompound comp = (NbtCompound) NbtFactory.fromItemTag(item);
        try {
            return comp.getInteger("RepairCost");
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }
    
}