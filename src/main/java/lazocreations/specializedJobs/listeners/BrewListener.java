package lazocreations.specializedJobs.listeners;

import com.comphenix.protocol.utility.MinecraftReflection;
import com.gmail.nossr50.runnables.skills.AlchemyBrewTask;
import com.gmail.nossr50.skills.alchemy.Alchemy;
import lazocreations.specializedJobs.Main;
import lazocreations.specializedJobs.data.PlayerDB;
import lazocreations.specializedJobs.data.PlayerJob;
import lazocreations.specializedJobs.exception.SkillDataCorruptedException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.BrewingStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class BrewListener implements Listener {
    
    private HashMap<Block, Integer> trackedBrewers = new HashMap<>();
    private List<BrewingStand> finishedBrewers = new ArrayList<>();
    private boolean mcmmoActive;
    
    public BrewListener(boolean mcMMOActive) {
        this.mcmmoActive = mcMMOActive;
    }
    
    @EventHandler
    public void onBrewDone(BrewEvent event) {
        
        BrewingStand brew = (BrewingStand) event.getBlock().getState();
        Logger log = Bukkit.getLogger();
        int time = brew.getBrewingTime();
        
        if(!finishedBrewers.contains(brew)) {
            event.setCancelled(true);
        }
        else {
            finishedBrewers.remove(brew);
            event.setCancelled(false);
        }
        
    }
    
    public void onClick(final InventoryClickEvent event) {
        
        BrewerInventory inv = (BrewerInventory) event.getInventory();
        final int rawSlot = event.getRawSlot();
        
        if(trackedBrewers.get(inv.getHolder().getBlock()) != null) {
            return;
        }
        
        if(!event.isShiftClick()) {
            if(event.getView().convertSlot(rawSlot) != rawSlot || !event.getAction().toString().startsWith("PLACE")) {
                return;
            }
        }
        
        Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), () -> {
            
            Player player = (Player) event.getWhoClicked();
            InventoryView view1 = player.getOpenInventory();
            
            if(view1 == null || !(view1.getTopInventory() instanceof BrewerInventory))
            { return; }
            
            BrewerInventory inv1 = (BrewerInventory) view1.getTopInventory();
            
            if(isBrewable(inv1) && consumeFuel(player, inv1.getHolder())) {
                Main.debugPlayer(player, "Start tracking brewing stand.");
                startTracking(inv1, view1);
            }
            else {
                BrewingStand stand = inv1.getHolder();
                player.sendMessage("You lack of ingredient or fuel.");
                event.setCancelled(true);
            }
            
        }, 1L);
        
    }
    
    public void onDrag(InventoryDragEvent event) {
        
        if(event.getRawSlots().size() > 1) {
            for (Integer i : event.getRawSlots()) {
                if(i == 3) {
                    event.setCancelled(true);
                    return;
                }
            }
        }
        
        else if(event.getRawSlots().iterator().next() == 3) {
            onClick(new InventoryClickEvent(event.getView(), InventoryType.SlotType.CONTAINER,
                    3, ClickType.RIGHT, InventoryAction.PLACE_ONE));
        }
        
    }
    
    private void startTracking(final BrewerInventory inv, final InventoryView view) {
        
        Block brewery = inv.getHolder().getBlock();
        
        trackedBrewers.put(brewery, new BukkitRunnable() {
            
            double duration = getBrewingTime((Player) view.getPlayer());
            double time = -1;
            Logger log = Bukkit.getLogger();
            
            @Override
            public void run() {
            
                BlockState state = brewery.getState();
                Player player = (Player) view.getPlayer();
            
                if(state instanceof BrewingStand) {
                    BrewingStand stand = (BrewingStand) state;
                    int fuel = stand.getFuelLevel();
                    
                    if(time > -1) {
                        time = isBrewable(stand.getInventory()) ? time - (400 / duration) : -1;
                        stand.setBrewingTime((int) Math.floor(time));
                        stand.update();
                        
                        if(time <= 1 && time > -1) {
                            time = -1;
                            finishedBrewers.add(stand);
                        }
                        
                        Main.debugPlayer(player, "Fuel: " + fuel + ", Brew time: " + stand.getBrewingTime());
                    }
                    else if(duration > 0 && stand.getBrewingTime() > 0) {
                        time = 400;
                        killMMOTask(stand);
                    }
                    else {
                        this.cancel();
                        trackedBrewers.remove(brewery);
                    }
                    
                }
                else {
                    this.cancel();
                    trackedBrewers.remove(brewery);
                }
            
            }
        
        }.runTaskTimer(Main.getPlugin(Main.class), 0L, 1L).getTaskId());
        
    }
    
    private boolean killMMOTask(BrewingStand stand) {
        if(mcmmoActive) {
            Location key = stand.getLocation();
            AlchemyBrewTask task = Alchemy.brewingStandMap.get(key);
            if(task != null) {
                task.cancel();
                Alchemy.brewingStandMap.remove(key);
                return true;
            }
        }
        return false;
    }
    
    private boolean consumeFuel(Player player, BrewingStand stand) {
    
        List<Map<?, ?>> mapList = Main.config.getMapList("skills.BREWING");
        PlayerJob.Ability skill = PlayerJob.Ability.BREWING;
        int grade = PlayerDB.getInstance().getAbilityGrade(player.getUniqueId(), skill);
        Integer fuel = null;
        
        for(Map<?, ?> map : mapList) {
            int grade1 = (Integer) map.get("grade");
            
            if(grade == grade1) {
                fuel = (Integer) map.get("fuel-charge");
            }
        }
        
        if(fuel == null) {
            try {
                throw new SkillDataCorruptedException(grade, skill.toString());
            } catch (SkillDataCorruptedException e) {
                e.printStackTrace();
            }
            return false;
        }
        
        fuel = stand.getFuelLevel() - (fuel - 1);
        
        if(fuel >= 0) {
            stand.setFuelLevel(fuel);
            stand.update(true);
            return true;
        }
        
        return false;
        
    }
    
    private boolean isBrewable(BrewerInventory inv) {
    
        BrewingStand stand = inv.getHolder();
        ItemStack ingred = inv.getIngredient();
        boolean bIngred = ingred != null && ingred.getType() != Material.AIR;
        int bottles = 0;
        
        for(int i=0; i<3; i++) {
            ItemStack item = inv.getContents() [i];
            if(item != null && item.getType() != Material.AIR) {
                bottles++;
            }
        }
        
        return bIngred && bottles >= 1;
        
    }
    
    private int getBrewingTime(Player player) {
        PlayerJob.Ability skill = PlayerJob.Ability.BREWING;
        List<Map<?, ?>> mapList = Main.config.getMapList("skills.BREWING");
        int grade = PlayerDB.getInstance().getAbilityGrade(player.getUniqueId(), skill);
        
        for(Map<?, ?> map : mapList) {
            if((Integer) map.get("grade") == grade) {
                return (Integer) map.get("brew-time") * 20;
            }
        }
        
        try {
            throw new SkillDataCorruptedException(grade, skill.toString());
        } catch (SkillDataCorruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    @Deprecated
    private Object getBrewingStandTile(BrewingStand stand)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        
        // ClassNotFound exception occurres
        Object craftBrewingStand = MinecraftReflection.getMinecraftClass("CraftBrewingStand").cast(stand);
        
        return craftBrewingStand.getClass().getMethod("getTileEntity").invoke(craftBrewingStand);
    }
    
}