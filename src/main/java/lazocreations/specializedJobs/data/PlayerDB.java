package lazocreations.specializedJobs.data;

import lazocreations.specializedJobs.Main;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PlayerDB {
	
	private static PlayerDB db;
	
	static {
		db = new PlayerDB();
	}
	
	public static PlayerDB getInstance() {
		return db;
	}
	
	private Connection connection = null;
	
	private PreparedStatement setJob;
	private PreparedStatement getJob;
	private PreparedStatement hasPlayerJob;
	private PreparedStatement isAbilityExist;
	private PreparedStatement addPlayer;
	private PreparedStatement addAbility;
	private PreparedStatement getAbility;
	private PreparedStatement setAbility;
	private PreparedStatement setDebug;
	private PreparedStatement getDebug;
	private PreparedStatement addData;
	
	private HashMap<UUID, Boolean> debugPlayers = new HashMap<>();
	
	private PlayerDB() {
		initialize();
	}
	
	private void initialize() {
		try {
			File dataFolder = Main.getPlugin(Main.class).getDataFolder();
			dataFolder.mkdirs();
			
			File dataFile = new File(dataFolder, "player.data");
			
			connection = DriverManager.getConnection("jdbc:sqlite:" + dataFile.getAbsolutePath());
			
			Statement createTable = connection.createStatement();
			createTable.setQueryTimeout(30);
			createTable.executeUpdate("CREATE TABLE IF NOT EXISTS playerJob (uuid TEXT PRIMARY KEY, job TEXT)");
			createTable.executeUpdate("CREATE TABLE IF NOT EXISTS playerAbility (uuid TEXT PRIMARY KEY, ability TEXT, upgrade INTEGER)");
			createTable.executeUpdate("CREATE TABLE IF NOT EXISTS playerData (uuid TEXT PRIMARY KEY, debug BOOLEAN)");
			createTable.close();
			
			setJob = connection.prepareStatement("UPDATE playerJob SET job = ? WHERE uuid = ?");
			getJob = connection.prepareStatement("SELECT job FROM playerJob WHERE uuid = ?");
			hasPlayerJob = connection.prepareStatement("SELECT 1 FROM playerJob WHERE uuid = ?");
			isAbilityExist = connection.prepareStatement("SELECT 1 FROM playerAbility WHERE uuid = ? AND ability = ?");
			addPlayer = connection.prepareStatement("INSERT INTO playerJob (uuid, job) VALUES (?, ?)");
			addAbility = connection.prepareStatement("INSERT INTO playerAbility (uuid, ability) VALUES (?, ?)");
			getAbility = connection.prepareStatement("SELECT upgrade FROM playerAbility WHERE uuid = ? AND ability = ?");
			setAbility = connection.prepareStatement("UPDATE playerAbility SET upgrade = ? WHERE uuid = ? AND ability = ?");
			getDebug = connection.prepareStatement("SELECT debug FROM playerData WHERE uuid = ?");
			setDebug = connection.prepareStatement("UPDATE playerData SET debug = ? WHERE uuid = ?");
			addData = connection.prepareStatement("INSERT INTO playerData (uuid, debug) VALUES (?, ?)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public void close() {
		try {
			if (connection != null) {
				setJob.close();
				getJob.close();
				hasPlayerJob.close();
				isAbilityExist.close();
				addPlayer.close();
				addAbility.close();
				getAbility.close();
				setAbility.close();
				getDebug.close();
				setDebug.close();
				
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Deprecated
	public void migrate() {
		try {
			Statement statement = connection.createStatement();
			
			statement.executeUpdate("ALTER TABLE playerJob RENAME TO player_tmp");
			statement.executeUpdate("CREATE TABLE playerJob (uuid TEXT PRIMARY KEY, job TEXT)");
			statement.executeUpdate("INSERT INTO playerJob (uuid, job) SELECT uuid, job FROM player_tmp");
			statement.executeUpdate("DROP TABLE player_tmp");
			statement.close();
			// or simply add new column and then use that.
			// ex.) ALTER TABLE player ADD COLUMN colName REAL
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized PlayerJob getPlayerJobType(UUID uuid) {
		try {
			getJob.setString(1, uuid.toString());
			ResultSet rs = getJob.executeQuery();
			getJob.clearParameters();
			
			if (rs.next()) { return PlayerJob.valueOf(rs.getString("job")); }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public synchronized int getAbilityGrade(UUID uuid, PlayerJob.Ability ability) {
		int grade = 5;
		
		try {
			getAbility.setString(1, uuid.toString());
			getAbility.setString(2, ability.toString());
			ResultSet rs = getAbility.executeQuery();
			getAbility.clearParameters();
			
			if (rs.next()) { grade -= rs.getInt("upgrade"); }
			
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (ability.getMasterJob().equals(getPlayerJobType(uuid))) {
			grade -= 3;
		}
		
		return Math.min(Math.max(grade, 1), 5);
	}
	
	public synchronized boolean setPlayerJobType(UUID uuid, PlayerJob type) {
		try {
			if (!hasPlayerJob(uuid)) addPlayer(uuid);
			
			setJob.setString(1, type.toString());
			setJob.setString(2, uuid.toString());
			setJob.executeUpdate();
			setJob.clearParameters();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public synchronized boolean setAbilityGrade(UUID uuid, PlayerJob.Ability ability, int grade) {
		String id = uuid.toString();
		String ab = ability.toString();
		
		if(ability.getMasterJob().equals(getPlayerJobType(uuid))) {
			grade += 3;
		}
		
		int upgrade = 5 - Math.min(Math.max(grade, 1), 5);
		
		try {
			isAbilityExist.setString(1, id);
			isAbilityExist.setString(2, ab);
			
			if (!isAbilityExist.executeQuery().next()) {
				addAbility.setString(1, id);
				addAbility.setString(2, ab);
				addAbility.executeUpdate();
				addAbility.clearParameters();
			}
			
			isAbilityExist.clearParameters();
			setAbility.setInt(1, upgrade);
			setAbility.setString(2, id);
			setAbility.setString(3, ab);
			setAbility.executeUpdate();
			setAbility.clearParameters();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public synchronized boolean hasPlayerJob(UUID uuid) {
		try {
			hasPlayerJob.setString(1, uuid.toString());
			
			boolean ret = hasPlayerJob.executeQuery().next();
			hasPlayerJob.clearParameters();
			return ret;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public synchronized boolean addPlayer(UUID uuid) {
		try {
			addPlayer.setString(1, uuid.toString());
			addPlayer.setString(2, PlayerJob.NONE.toString());
			addPlayer.executeUpdate();
			addPlayer.clearParameters();
			addData.setString(1, uuid.toString());
			addData.setBoolean(2, false);
			addData.executeUpdate();
			addData.clearParameters();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public synchronized boolean setDebug(UUID uuid, boolean debug) {
		try {
			setDebug.setBoolean(1, debug);
			setDebug.setString(2, uuid.toString());
			setDebug.executeUpdate();
			setDebug.clearParameters();
			debugPlayers.put(uuid, debug);
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public synchronized boolean getDebug(UUID uuid) {
		
		Boolean debug = debugPlayers.get(uuid);
		
		if(debug != null) {
			return debug;
		}
		
		try {
			getDebug.setString(1, uuid.toString());
			ResultSet set = getDebug.executeQuery();
			
			if(set.next()) {
				debug = set.getBoolean("debug");
				debugPlayers.put(uuid, debug);
				return debug;
			}
			
			getDebug.clearParameters();
			set.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public synchronized void purge() {
		try {
			Statement statement = connection.createStatement();
			
			statement.executeUpdate("DELETE FROM playerJob WHERE job = NULL");
			statement.executeUpdate("DELETE FROM playerAbility WHERE ability = 0");
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}